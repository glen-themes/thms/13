// stream griefling by essenger

var root = document.documentElement;
var rootGET = getComputedStyle(root); // follow with: getPropertyValue()
var rootSET = root.style; // follow with: setProperty()

var customize_page = window.location.href.indexOf("/customize") > -1;
var on_main = window.location.href.indexOf("/customize") < 0;

document.addEventListener("DOMContentLoaded",() => {
    var bob = document.getElementsByTagName("html")[0];

    if(customize_page){
        bob.setAttribute("customize-page","true");
    } else if(on_main){
        bob.setAttribute("customize-page","false");
    }
    
    var hdtzt = document.querySelectorAll("[header-texts]");
    if(hdtzt.length > 0){
        hdtzt[0].style.visibility = "visible";
    }
    
    var hfzqw = document.querySelectorAll(".header-sect")[0];
    hfzqw.classList.add("hfzqw")
    
    var cssgwam = rootGET.getPropertyValue("--Reblogger-Avatar-Filter").trim().slice(1).slice(0,-1);
    
    /*-----*/
    document.querySelectorAll("[reblogger-icon]").forEach(ojtpm => {
        var dxhzo = ojtpm.getAttribute("reblogger-icon");
        ojtpm.style.backgroundImage = "url(" + dxhzo + ")";
        
        if(cssgwam !== ""){
            ojtpm.classList.add("cssgram");
            ojtpm.classList.add(cssgwam);
        }
    });
    
    
});
    
// audio post play button - flaticon.com/free-icon/play-button_152770
var playb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Layer_1' x='0px' y='0px' viewBox='0 0 330 330' style='enable-background:new 0 0 330 330;' xml:space='preserve'> <path id='XMLID_497_' d='M292.95,152.281L52.95,2.28c-4.625-2.891-10.453-3.043-15.222-0.4C32.959,4.524,30,9.547,30,15v300 c0,5.453,2.959,10.476,7.728,13.12c2.266,1.256,4.77,1.88,7.272,1.88c2.763,0,5.522-0.763,7.95-2.28l240-149.999 c4.386-2.741,7.05-7.548,7.05-12.72C300,159.829,297.336,155.022,292.95,152.281z M60,287.936V42.064l196.698,122.937L60,287.936z'/> </svg>";

document.documentElement.style.setProperty('--audioplay','url("' + playb + '")');

// audio post pause button - flaticon.com/free-icon/pause_747384
var pauseb = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'> <g> <g> <path d='M154,0H70C42.43,0,20,22.43,20,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C204,22.43,181.57,0,154,0z M164,462c0,5.514-4.486,10-10,10H70c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> <g> <g> <path d='M442,0h-84c-27.57,0-50,22.43-50,50v412c0,27.57,22.43,50,50,50h84c27.57,0,50-22.43,50-50V50C492,22.43,469.57,0,442,0z M452,462c0,5.514-4.486,10-10,10h-84c-5.514,0-10-4.486-10-10V50c0-5.514,4.486-10,10-10h84c5.514,0,10,4.486,10,10V462z'/> </g> </g> </svg>";

document.documentElement.style.setProperty('--audiopause','url("' + pauseb + '")');

// audio post 'install audio' button
// feathericons
var cdrii = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><path d='M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4'></path><polyline points='7 10 12 15 17 10'></polyline><line x1='12' y1='15' x2='12' y2='3'></line></svg>";

document.documentElement.style.setProperty('--install','url("' + cdrii + '")');

// external link icon
// feathericons
var schtd = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-external-link'><path d='M18 13v6a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h6'/><polyline points='15 3 21 3 21 9'/><line x1='10' y1='14' x2='21' y2='3'/></svg>";

document.documentElement.style.setProperty('--ext','url("' + schtd + '")');
    
// 'previous page' svg
// feathericons
var prev = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='11 17 6 12 11 7'></polyline><polyline points='18 17 13 12 18 7'></polyline></svg>";

document.documentElement.style.setProperty('--BackSVG','url("' + prev + '")');
    
// 'next page' svg
// feathericons
var next = "data:image/svg+xml;charset=utf8,<svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='black' stroke-width='1.5' stroke-linecap='round' stroke-linejoin='round' class='feather feather-hash'><polyline points='13 17 18 12 13 7'></polyline><polyline points='6 17 11 12 6 7'></polyline></svg>";

document.documentElement.style.setProperty('--NextSVG','url("' + next + '")');

// glen svg
var glenSVG = '<svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="150.000000pt" height="150.000000pt" viewBox="0 0 150.000000 150.000000" preserveAspectRatio="xMidYMid meet"><g transform="translate(0.000000,150.000000) scale(0.100000,-0.100000)" fill="#000000" stroke="none"><path d="M555 1310 c-154 -61 -311 -123 -350 -137 -38 -14 -82 -38 -98 -52 l-27 -27 -1 -372 c0 -247 4 -378 11 -392 6 -11 23 -26 38 -34 59 -30 533 -216 552 -216 31 0 553 208 574 229 17 18 18 42 20 387 l1 368 105 41 c85 34 106 46 108 63 3 18 -4 26 -32 37 -19 8 -49 15 -65 15 -16 0 -183 -61 -371 -135 l-342 -136 -185 72 c-111 44 -179 75 -171 80 27 15 529 209 541 209 7 0 76 -25 152 -56 111 -45 147 -55 178 -51 52 7 90 33 79 54 -11 19 -342 151 -397 158 -34 5 -83 -11 -320 -105z m-122 -380 c126 -49 239 -90 252 -90 18 0 436 158 468 177 4 2 6 -141 5 -318 l-3 -323 -207 -82 -208 -82 0 168 c0 145 -2 171 -17 189 -10 11 -69 39 -130 61 l-113 41 -55 -17 c-89 -28 -75 -47 73 -101 l122 -45 0 -148 0 -148 -215 86 -215 86 0 318 c0 175 3 318 8 318 4 0 110 -40 235 -90z"></path></g></svg>';
// =>

$(document).ready(function(){
    // check jquery version
    var jqver = jQuery.fn.jquery;
    jqver = jqver.replaceAll(".","");
    
    $(".tumblr_preview_marker___").remove();
    
    // $("[reblogger-icon]").each(function(){
    //     var getrfdj = $(this).attr("reblogger-icon");
    //     $(this).css("background-image","url(" + getrfdj + ")")
    // })
    
    /*-------- TOOLTIPS --------*/
    $("a[title]:not([title=''])").style_my_tooltips({
        tip_follows_cursor:true,
        tip_delay_time:0,
        tip_fade_speed:0,
        attribute:"title"
    });
    
    /*------- NPF VIDEO STUFF -------*/
    $(".npf_inst").each(function(){
        if($(this).find("[data-npf*='video']").length){
            $(this).addClass("npf-video")
        }
    })
    
    $(".npf-video video").each(function(){
        $(this).attr("loop","")
    })
    
    /*---- NPF IMG ADJUSTMENTS FOR RP BLOGS ----*/
    $("[data-big-photo-width]").each(function(){
        var dbpw = parseInt($(this).attr("data-big-photo-width"));
        if(dbpw < 250){
            $(this).find("img").addClass("npf-small")
        }
    })
    
    /*---- GIF SRC ATTRIBUTION LINK ----*/
    $(".tmblr-attribution").each(function(){
        var peeop = $(this).find("[data-peepr]");
        var peepeeo = peeop.data("peepr");
        var gifP0STER = peepeeo.tumblelog;
        $(this).find("a").eq(0).text("GIF from @" + gifP0STER)
    })
    
    /*----------- NOSRC ADAPT -----------*/
    $(".nosrc").each(function(){
        if($(this).nextAll().find(".reblog-head").length){
            $(this).remove()
        } else {
            $(this).addClass("flex");
            $(this).css("visibility","visible")
        }
    })
    
    /*----------- REMOVE <p> WHITESPACE -----------*/
    $(".postinner > *").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $(".postinner p").each(function(){
        if(!$(this).prev().length){
            if($(this).parent().is(".postinner")){

                $(this).css("margin-top",0)
            }
        }
        
        if(!$(this).next().length){
            // target last <p>
            // if it's empty, remove
            if($.trim($(this).html()) == ""){
                $(this).remove();
            }
        }
    })
    
    $(".postinner p, .postinner blockquote, .postinner ol, .postinner ul").each(function(){
        if(!$(this).next().length){
            // target last <p>
            // if no next sibling, negate bottom padding
            $(this).css("margin-bottom",0)
        }
        
        if($(this).next().is(".tagsdiv")){
            $(this).css("margin-bottom",0)
        }
    })
    
    // remove empty captions
    $(".caption").each(function(){
        if($.trim($(this).text()) == ""){
            $(this).remove()
        }
    })
    
    if(customize_page){
        $(".reblog-comment > p[last-comment]:first-child").each(function(){
            $(this).css("margin-top",0)
        })
    }
    
    $("[original-post] .postinner").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $("[reblogged-post] .reblog-comment").each(function(){
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>")
    })
    
    $(".posts").each(function(){
        $(this).find("p, h1, h2, h3, h4, h5, h6, blockquote, ol, ul").each(function(){
            if(!$(this).prev().length){
                $(this).css("margin-top",0)
            }
            
            if(!$(this).next().length){
                $(this).css("margin-bottom",0)
            }
        })
    })
    
    $(".reblog-comment").each(function(){
        $(this).find("p").eq(0).each(function(){
            if(!$(this).prev().length){
                if($(this).css("margin-top") == "0px"){
                    $(this).css("margin-top","")
                }
            }
        })
    })
    
    /*----------- REBLOG-HEAD -----------*/
    $(".reblog-url").each(function(){
        var uz = $.trim($(this).text());
        if(uz.indexOf("-deac") > 0){
            var rogner = uz.substring(0,uz.lastIndexOf("-"));
            $(this).find("a").attr("href","//" + rogner + ".tumblr.com");
            $(this).find("a").text(rogner);
            $(this).append("<span class='deac'>(deactivated)</span>")
        }
    })
    
    /*-------- AUDIO BULLSH*T --------*/
    var mtn = Date.now();
    var fvckme = setInterval(function(){
        if(Date.now() - mtn > 1000){
            clearInterval(fvckme);
            $(".audiowrap").each(function(){
                $(this).prepend("<audio src='" + $(this).attr("audio-src") + "'>");
            });
            
            $(".inari").each(function(){
                var m_m = $(this).parents(".audiowrap").attr("audio-src");
                $(this).attr("href",m_m);
            })
        } else {
            $(".tumblr_audio_player").each(function(){
                if($(this).is("[src]")){
                    var audsrc = $(this).attr("src");
                    audsrc = audsrc.split("audio_file=").pop();
                    audsrc = decodeURIComponent(audsrc);
                    audsrc = audsrc.split("&")[0];
                    $(this).parents(".audiowrap").attr("audio-src",audsrc)
                }
            })
        }
    },0);
    
    $(".albumwrap").click(function(){
        
        var emp = $(this).parents(".audiowrap").find("audio")[0];
        
        if(emp.paused){
            emp.play();
            $(".overplay",this).addClass("ov-z");
            $(".overpause",this).addClass("ov-y");
        } else {
            emp.pause();
            $(".overplay",this).removeClass("ov-z");
            $(".overpause",this).removeClass("ov-y");
        }
        
        var that = this
        
        emp.onended = function(){
            $(".overplay",that).removeClass("ov-z");
            $(".overpause",that).removeClass("ov-y");
        }
    })
    
    // minimal soundcloud player @ shythemes.tumblr
    var soundcolor = rootGET.getPropertyValue("--Body-Text-Color");
    
    $('iframe[src*="soundcloud.com"]').each(function(){
        $(this).one("load",function(){
            soundfvk()
        });
    });
    
    function soundfvk(){
       $('iframe[src*="soundcloud.com"]').each(function(){
           $(this).attr({ src: $(this).attr('src').split('&')[0] + '&amp;liking=false&amp;sharing=false&amp;auto_play=false&amp;show_comments=false&amp;continuous_play=false&amp;buying=false&amp;show_playcount=false&amp;show_artwork=true&amp;origin=tumblr&amp;color=' + soundcolor.split('#')[1], height: 116, width: '100%' });
       });
    }
    
    $(".soundcloud_audio_player").each(function(){
        $(this).wrap("<div class='audio-soundcloud'>")
    })
    
    /*-------- NPF QUOTE POSTS --------*/
    // TRY to identify them, anyway
    $(".op-blockquote").each(function(){
        var bbqDIV = $(this).children("div:not([class])").eq(0);
        var bbqP = $(this).next("p");
        if(!bbqDIV.siblings().length){
            if(bbqP.length){
                if(bbqP.is('[style*="margin-bottom: 0px;"]')){
                    bbqDIV.addClass("quote-words").unwrap();
                    
                    if(!bbqDIV.prev(".quote-ico").length){
                        bbqDIV.before("<div class='quote-ico'><i lucide icon-name='quote'></i></div>");
                        lucide.createIcons();
                    }
                    
                    bbqP.addClass("quote-source");
                }
            }
        }
    })
    
    /*-------- QUOTE SOURCE BS --------*/
    $(".quote-source").each(function(){
        
        var tntz = $(this).html();
        tntz = tntz.replaceAll("(via ","").replaceAll("(Source: ","")
        $(this).html(tntz);
        
        $(this).find("a.tumblr_blog").remove();
        $(this).find("a[href*='tumblr.com']").remove();
        
        $(this).add($(this).find("p")).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<span/>");
        
        $(this).find("span:not([class])").each(function(){
            if(!$(this).next().length){
                if($.trim($(this).text()) == ")"){
                    $(this).remove();
                }
            }
        })
    })
    
    /*-------- ASK/ANSWER POSTS --------*/
    $(".question_text").each(function(){
        if(!$(this).children().first().is("p")){
            $(this).wrapInner("<p></p>")
        }
    })
    
    /*-------- CHAT POSTS --------*/
    $(".npf_chat").each(function(){
        $(this).find("b").each(function(){
            var cb = $(this).html();
            $(this).before("<div class='chat_label'>" + cb + "</div>");
            $(this).remove()
        })
        
        $(this).contents().filter(function(){
            return this.nodeType == 3 && this.data.trim().length > 0
        }).wrap("<div class='chat_content'>");
        
        $(this).wrap("<div class='chat_row'>");
        $(this).children().unwrap()
    })
    
    $(".chat_row").each(function(){
        $(this).not(".chat_row + .chat_row").each(function(){
            if(jqver < "180"){
                $(this).nextUntil(":not(.chat_row)").andSelf().wrapAll('<div class="chatwrap">');
            } else {
                $(this).nextUntil(":not(.chat_row)").addBack().wrapAll('<div class="chatwrap">');
            }
        });
    })
    
    /*---- MAKE SURE <p> IS FIRST CHILD OF RB ----*/
    $(".reblog-head").each(function(){
        if(!$(this).next(".reblog-comment").length){
            $(this).nextUntil(".tagsdiv").wrapAll("<div class='reblog-comment'>")
        }
    })
    
    $(".reblog-comment").each(function(){
        if($(this).children().first().is("div")){
            $(this).prepend("<p></p>")
        }
        
        if(!$(this).prev().length){
            if($(this).children().first().is("p")){
                if($.trim($(this).find("p:first").text()) == ""){
                    $(this).find("p:first").remove();
                }
            }
        }
    })
    
    /*-------- CLICKTAGS --------*/
    var tags_ms = parseInt($.trim(rootGET.getPropertyValue("--Tags-Fade-Speed")));
    
    $(".clicktags").hover(function(){
        if(!$(this).hasClass("clique")){
            $(this).attr("title","show tags")
        } else {
            $(this).attr("title","hide tags")
        }
    });
    
    
    $(".clicktags").click(function(){
        var that = this;
        var tagsdiv = $(this).parents(".permadiv").prev(".tagsdiv");
        
        if(!$(this).hasClass("clique")){
            $(this).addClass("clique");
            tagsdiv.slideDown(tags_ms);
            setTimeout(function(){
                tagsdiv.addClass("tagsfade");
            },tags_ms);
        } else {
            tagsdiv.removeClass("tagsfade");
            setTimeout(function(){
                tagsdiv.slideUp(tags_ms);
                $(that).removeClass("clique");
            },tags_ms)
        }
    })
    
    /*---- remove lengthy tumblr redirects ----*/
    // credit: @magnusthemes
    // part 1/2
    $('a[href*="t.umblr.com/redirect"]').each(function(){
      var originalURL = $(this).attr("href").split("?z=")[1].split("&t=")[0];
      var replaceURL = decodeURIComponent(originalURL);
      $(this).attr("href", replaceURL);
    });
    
    // part 2/2
    function noHrefLi(){
        var linkSet = document.querySelectorAll('a[href*="href.li/?"]');
        Array.prototype.forEach.call(linkSet,function(el,i){
            var theLink = linkSet[i].getAttribute('href').split("href.li/?")[1];
            linkSet[i].setAttribute("href",theLink);
        });
    }
    noHrefLi();
    
    /*--- make iframe heights look more 'normal' ---*/
	$(".embed_iframe").each(function(){
        if($(this).parent().is(".tmblr-embed")){
            var wut = $(this).width();
            
            var wrath = $(this).attr("width");
            var rat_w = wrath / wut;
            
            var hrath = $(this).attr("height");
            var rat_h = hrath / rat_w;
            
            $(this).height(rat_h)
        }
    })
    
    /*--- prevent display:none from "empty" images ---*/
    var imgs = document.querySelectorAll("img");
    Array.prototype.forEach.call(imgs, function(invis){	
      if(invis.src.indexOf("assets.tumblr.com/images/x.gif") > -1){
        invis.setAttribute("src","https://cdn.glitch.com/bdf00c8f-434a-46d9-a514-ec8332ec176a/1x1.png");
      }
    });
    
    /*----- OTHER -----*/
    $(".chat_content").each(function(){
        if($.trim($(this).text()).indexOf("{block:") > -1){
            var notgod = $(this).html();
            notgod.replaceAll("{","&lcub;").replaceAll("}","&rcub;");
            $(this).before("<code>" + notgod + "</code>");
            $(this).remove()
        }
    })
    
    /*----- POST SHADOW -----*/
    // fallback if user didn't put "px"
    var pstsdsz = $.trim(rootGET.getPropertyValue("--Post-Shadow-Size"));
    if(/^\d+$/.test(pstsdsz)){
        rootSET.setProperty("--Post-Shadow-Size", pstsdsz + "px");
    }
    
    var trsyl = $.trim(rootGET.getPropertyValue("--Post-Shadow-Strength"));
    if(/^\d+$/.test(trsyl)){
        rootSET.setProperty("--Post-Shadow-Strength", trsyl + "%");
    }
    
    var noozj = $.trim(rootGET.getPropertyValue("--Post-Shadow-Blur"));
    if(/^\d+$/.test(noozj)){
        rootSET.setProperty("--Post-Shadow-Blur", noozj + "px");
    }
    
    var yfuty = $.trim(rootGET.getPropertyValue("--Container-Shift-Amount"));
    if(/^\d+$/.test(yfuty)){
        rootSET.setProperty("--Container-Shift-Amount", yfuty + "px");
    }
    
    /*----- HEADER TEXT -----*/
    if($("[header-texts]").length){
    
        var txtcols = parseInt(rootGET.getPropertyValue("--Header-Info-Columns"));
        
        $("[header-texts] li").each(function(){
            var littz = $.trim($(this).html());
            littz = littz.replaceAll("[ICON-NAME]","[icon-name]").replaceAll("[BOLD-TEXT]","[bold-text]").replaceAll("[TEXT]","[text]");
            littz = littz.replaceAll("[icon-name]","<i class='ai-'></i>");
            littz = littz.replaceAll("[bold-text]","<span label></span>");
            littz = littz.replaceAll("[text]","<span deet-text></span>");
            
            $(this).html(littz);
            
            $(this).contents().filter(function(){
                return this.nodeType == 3 && this.data.trim().length > 0
            }).wrap("<span str/>");
            
            // icon name
            $(this).find("[class*='ai-']").each(function(){
                var vmqqe = $.trim($(this).next("[str]").text());
                vmqqe = vmqqe.replaceAll(" ","-");
                $(this).addClass("ai-" + vmqqe).removeClass("ai-");
                $(this).next("[str]").remove();
            })
            
            // label name
            $(this).find("[label]").each(function(){
                $(this).nextUntil("[deet-text]").appendTo($(this))
            })
            
            // detail text
            $(this).find("[deet-text]").each(function(){
                $(this).nextUntil("[class*='ai-']").appendTo($(this));
            })
            
        })// end all lis
        
        // attempt to wrap the icon and the LABEL name only
        $("[ht-col] [class*='ai-'] + [label]").each(function(){
            $(this).add($(this).prev()).wrapAll("<div htl-wrap></div>")
        })
    
    }//end if header texts exist
    
    /*--- MUSIC PLAYER ---*/
    if($("#audio").length){
        
        var aaa = document.getElementById("audio");
        var reeea = $("#audio");
        
        // music volume
        if(reeea.is("[volume]")){
            var aavol = $.trim(reeea.attr("volume"));
            if(aavol !== ""){
                if(aavol.indexOf("%")){
                    aavol = parseInt(aavol) / 100;
                }
                
                aaa.volume = aavol;
                reeea.attr("parsed-vol",aavol);
            }
        }
    }
    
    /*--- CHAD ---*/
    $(".glenbiz").each(function(){
        $(this).prepend(glenSVG)
    })
    
    if(!$("[boxlinks] a[href*='//glenthemes.tumblr.com']").length){
        $("body").append("<a ccdfo href=\'//glenthemes.tumblr.com\' title=\'&#x2740;&#8201;parfait&#8201;&#x2740; theme &#8202;&#8212;&#8202; @glenthemes\'>" + glenSVG + "</a>")
    }
    
    /*--- CUSTOMLINKS 'PLUS' CLICK ---*/
    var clSPEED = parseInt($.trim(rootGET.getPropertyValue("--CustomLinks-Fade-Speed")));
    
    $(".cl-tog").click(function(){
        var that = this;
        var CLdiv = $(that).parents(".envzk").next("[krryo]");
        
        if(!$(this).hasClass("fcovn")){
            $(this).addClass("fcovn");
            
            CLdiv.slideDown(clSPEED);
            setTimeout(function(){
                CLdiv.addClass("cl-opac")
            },clSPEED)
            
        } else {
            $(this).removeClass("fcovn");
            
            CLdiv.removeClass("cl-opac");
            setTimeout(function(){
                CLdiv.slideUp(clSPEED)
            },clSPEED)
        }
    })
    
    /*--- CUSTOMLINKS MAKE ROWS ---*/
    // how many columns
    var clCOLS = parseInt($("[clpr]").attr("clpr"));
    
    // note to self:
    // logic: its.gd/z/Ulpl-
    
    for(var eek=1; eek<clCOLS+1; eek++){
        
        // generate the columns u will later put stuff in
        $("<div customlink-col cl-col-" + eek + "></div>").insertBefore($("[customlink]:first"));
        
        // assign the starting numbers of the designated col count
        $("[efrhc] [customlink]:nth-of-type(" + eek + ")").each(function(){
            $(this).attr("col-id",eek);
        })
        
        // auto assign the rest based on nth-of-type
        var neek = eek + clCOLS;
        $("[efrhc] [customlink]:nth-of-type(" + clCOLS + "n+" + neek + ")").each(function(){
            $(this).attr("col-id",eek)
        })
    }
    
    // once the [col-id]s have done the thing,
    // put them into the column that matches the their ID num
    // this must be OUTSIDE the forLoop
    $("[col-id]").each(function(){
        var collid = $(this).attr("col-id");
        $(this).appendTo($(this).parent().find("[cl-col-" + collid + "]"))
    })
    
    // lastly, count how many have been sorted into each column
    // (just in case they're  uneven)
    var uhhh = $("[customlink-col]").map(function(){
        return $(this).find("[col-id]").length;
    }).get();
    
    var perfROWS = Math.min.apply(Math,uhhh);
    $("[customlink-col]").eq(0).parent().attr("perfect-rows",perfROWS);
    
    // single out the columns that have extra shit in them
    $("[customlink-col]").each(function(){
        var xkdhc = $(this).find("[col-id]").length;
        if(xkdhc > perfROWS){
            $(this).addClass("non-perf")
            
            var lazytem = $(this).find("[col-id]:last");
            var lazyID = parseInt(lazytem.attr("col-id"));
        }
    })
    
    // establish the remaining number var
    var besussy = $("[perfect-rows]").find("[customlink-col]:not(.non-perf)").length;
    $("[perfect-rows]").attr("extras-shift-var",besussy);
    
    // move the extra items in columns to the rightside
    $(".non-perf").each(function(){
        $(this).find("[customlink]:last").each(function(){
            var freja = parseInt($(this).attr("col-id"));
            var mxth = freja + besussy;
            $(this).attr("mxth",mxth);
            
            $(this).appendTo($(this).parent(".non-perf").siblings("[customlink-col][cl-col-" + mxth + "]"))
        })
    })
    
    /*-----------*/
    
    if($("[customlink]").length){
        var xgnyl = Date.now();
        var sgurn = setInterval(function(){
            if(Date.now() - xgnyl > 6699){
                clearInterval(sgurn);
            } else {
                if($("[customlink-col]").length){
                    $("[cls='show'] [krryo]").show();
                }
            }
        },0);
    }
    
    // fallback for CLs don't show even when page has loaded
    $(window).load(function(){
        $("[cls='show'] [krryo]").show();
    })
    
    // customize msg
    if(customize_page){
        $("body").prepend('<div glvwj><div uutsa> <div class="stinson"> <img src="https://static.tumblr.com/gtjt4bo/5ySs9hqu4/jolyne_door.gif"> </div><h1>Hello! Thanks for using this theme!</h1> <p>Please read through <a guide-link href=\"//docs.google.com/presentation/d/1jv8m7GLZWYv9-xrkmFFtfaRyxmEgDkbfn1WZ9cS8hgM/edit?usp=sharing\" target=\"_blank\">the guide</a> I wrote for you! It tells you where and how to edit things. If you have any questions, feel free to use the <b>#theme-help</b> channel on my <a href=\"https://discord.gg/RcMKnwz\">Discord</a> or DM me once you\'re there.<div class="burp"><a class="burpa">okay, got it.</a></div></div></div>')
        
        $(window).load(function(){
            $("[glvwj]").addClass("bcoec")
        })
        
        $(".burpa").click(function(){
            $("[glvwj]").fadeOut(269)
        })
        
        /*--------------*/
        
        $("body").prepend('<div rvjck flex> <i class="mi-circle-help"></i> <span>need help?</span></div>');
        
        $("[rvjck]").click(function(){
            $("[glvwj]").css({
                "display":"table",
                "animation-delay":"0s"
            })
        })
    }
});//end jquery / end ready
