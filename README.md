![Screenshot preview of the theme "Parfait" by glenthemes](https://64.media.tumblr.com/377b53feb0c133c19a140154f2433ffa/2565f7f63478128e-3f/s1280x1920/c3300d233b12038189959027686988f922576d3a.gif)

**Theme no.:** 13  
**Theme name:** Parfait  
**Theme type:** Free / Tumblr use  
**Description:** <small>PARFAIT</small> is a cute, minimal theme with optional click-to-show sidebar links, and a header section to put brief info.  
**Author:** @&hairsp;glenthemes  

**Release date:** [2015-11-??](https://64.media.tumblr.com/e5c5d7fae1b3d6d9be3c3ef57d17496b/tumblr_ntu8mrvtY91ubolzro1_1280.png)  
**Rework date [v1]:** [2020-03-23](https://64.media.tumblr.com/c6a7a55d822a76625d7e329c11c4795d/2565f7f63478128e-39/s1280x1920/1b079ebca58835e516679fd5cdc11d8f40c2afc9.png)  
**Rework date [v2]:** 2022-05-10

**Post:** [glenthemes.tumblr.com/post/613346587918401536](https://glenthemes.tumblr.com/post/613346587918401536)  
**Preview:** [glenthpvs.tumblr.com/parfait](https://glenthpvs.tumblr.com/parfait)  
**Download:** [pastebin.com/kH03d6DP](https://pastebin.com/kH03d6DP)  
**Guide:** [docs.google.com/presentation/d/1jv8m7GLZWYv9-xrkmFFtfaRyxmEgDkbfn1WZ9cS8hgM/edit?usp=sharing](https://docs.google.com/presentation/d/1jv8m7GLZWYv9-xrkmFFtfaRyxmEgDkbfn1WZ9cS8hgM/edit?usp=sharing)  
**Credits:** [glencredits.tumblr.com/parfait](https://glencredits.tumblr.com/parfait)
